
public class Application
{
	public static void main(String []args)
	{
		Dogs dog1 = new Dogs("black", 32.02, 5);
		System.out.println("Color: "+ dog1.getColor()+ " Speed (after setname): "+dog1.getSpeed()+" Age: "+dog1.getAge());
		
		Dogs dog2 = new Dogs("yellow", 22.02, 1550);
		System.out.println("Color: "+ dog2.getColor()+ " Speed: "+dog2.getSpeed()+" Age: "+dog2.getAge());
		
		dog1.doJump();
		dog1.doRun();
		dog1.growUp();
		
		System.out.println("New age of dog1: "+ dog1.getAge());
		
	}
}