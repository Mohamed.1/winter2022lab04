
public class Dogs
{
	private String color;
	private double speed;
	private int age;
	private int height;
	
	public void doJump()
	{
		System.out.println("Do a jump at "+speed);
	}
	
	public void doRun()
	{
		if(this.color.equals("black"))
		{
		System.out.println("Dog ran at "+speed); 
		}
	}
	
	public void growUp()
	{
		age++;
	}
	
	public String getColor()
	{
		return this.color;
	}

	public double getSpeed()
	{
		return this.speed;
	}	
	
	public int getAge()
	{
		return this.age;
	}
	
	public int getHeight()
	{
		return this.height;
	}
	
	public void setSpeed(double newSpeed)
	{
		this.speed = newSpeed;
	}
	
	public void setAge(int newAge)
	{
		this.age = newAge;
	}
	
	public Dogs(String color, double speed, int age)
	{
			this.color = color;
			this.speed = speed;
			this.age = age;
			this.height = height;
	}
}
