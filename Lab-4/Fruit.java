public class Fruit
{
	private String type;
	private String color;
	private int height;

	/*-------------------------------------------------------------------------------------------------*/
	public String getType()
	{
		return this.type;
	}	

	public String getColor()
	{
		return this.color;
	}	
	
	public int getHeight()
	{
		return this.height;
	}	
	
	public void setType(String newType)
	{
		this.type = newType;
	}
	
	public void setColor(String newColor)
	{
		this.color = newColor;
	}
	
	/* public void setHeight(int newHeight)
	{
		this.height = newHeight;
	}
	*/
	
	/*----------------------------------------------------------------------------------------------------*/
	public void getTheHeight()
	{	if(this.type.equals("banana"))
		{
			System.out.println("It is "+height+ " cm long"); 
		}
	}
	
	/*-------------------------------------------------------------------------------------------------------*/
	public Fruit(String type, String color, int height)
	{
			this.type = type;
			this.color = color;
			this.height = height;
	}
}
