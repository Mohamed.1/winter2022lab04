import java.util.*;

public class Shop
{
	public static void main(String []args)
	{
		String theType;
		String theColor;
		int theHeight;
			
		Scanner scan = new Scanner(System.in);
		Fruit[] fruits = new Fruit[4];
		for(int i = 0; i < fruits.length ; i++)
		{
			System.out.println("Part "+(1+i));
			System.out.println("Enter the type of fruit (no cap)");
			theType = scan.nextLine();
			System.out.println("Enter the color of the fruit");
			theColor = scan.nextLine();
			System.out.println("Enter the height of the fruit");
			theHeight = Integer.parseInt(scan.nextLine());
			fruits[i] = new Fruit( theType, theColor, theHeight);
		}
		System.out.println("Type of last fruit: "+fruits[3].getType());
		System.out.println("Color of last fruit: "+fruits[3].getColor());
		System.out.println("Height of last fruit: "+fruits[3].getHeight());
		
		System.out.println("Enter the new type of the last fruit(no cap)");
		fruits[3].setType(scan.nextLine());
		System.out.println("Enter the new color of the fruit");
		fruits[3].setColor(scan.nextLine());
	
		System.out.println("Type of last fruit: "+fruits[3].getType());
		System.out.println("Color of last fruit: "+fruits[3].getColor());
		System.out.println("Height of last fruit: "+fruits[3].getHeight());
	}		
}